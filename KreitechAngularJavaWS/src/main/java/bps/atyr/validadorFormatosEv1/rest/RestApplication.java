package bps.atyr.validadorFormatosEv1.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("rest")
public class RestApplication extends Application
{
    @Override
    public Set<Class<?>> getClasses()
    {
        final Set<Class<?>> classes = new HashSet<>(1);
        classes.add(UtilitarioRestServiceWeb.class);
        return classes;
    }
}