package bps.atyr.validadorFormatosEv1.rest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataOutput;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;


import org.apache.http.client.ClientProtocolException;

import bps.atyr.acerca.utils.ResourceHelper;
import bps.atyr.acerca.utils.logueo.LogueoAplicacion;
//import bps.atyr.acerca.validadorFormatosEv1.common.ejb.interfaces.IFacadeWebEJB;
//import bps.atyr.acerca.validadorFormatosEv1.common.msg.Mensaje;
import bps.atyr.kformato.utils.Resultado;
import bps.atyr.validadorFormatosEv1.ejb.FacadeWebEJB;

import org.json.simple.JSONObject;


@Stateless
@Path("/utilitarios")
@Produces(MediaType.APPLICATION_JSON)
public class UtilitarioRestServiceWeb {

	private static Logger logger = Logger.getLogger(UtilitarioRestServiceWeb.class);

	public final static String USUARIO = "usuario";
	
	
	@EJB
	FacadeWebEJB facadeWebEJB;
		
	@Path("/conf.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject obtEmpresasUsuario(@Context final HttpServletRequest request,@Context final HttpServletResponse responde) {
		responde.setHeader("Access-Control-Allow-Origin", "*");
	
		
		JSONObject obj = new JSONObject();
        obj.put("host", "http://fso13p0232w076.bps.net");
        obj.put("port", 8080);
        obj.put("contextRoot", "ValidadorFormatosWebEv1WS");
		//return "{"host": "http://fso13p0232w076.bps.net","port": "8080","contextRoot": "ValidadorFormatosWebEv1WS"}";
        return obj;
	
	}
	
	
	@Path("/site-key")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject siteKey(@Context final HttpServletRequest request,@Context final HttpServletResponse responde) {
		responde.setHeader("Access-Control-Allow-Origin", "*");
	
		
		  FacadeWebEJB facadeWebEJB =  null;
    	  try {
    		 InitialContext ctx = new InitialContext();
    		 facadeWebEJB = (FacadeWebEJB) ctx.lookup(ResourceHelper.getConfig("jndiIFacadeWebEJB"));
    	  
	 	  //RECAPTCHA
    	  ClientRequest request1 = new ClientRequest(facadeWebEJB.getSiteKeyUrl());
    	  request1.accept("application/json");
 		  ClientResponse<JSONObject> response1 = request1.get(JSONObject.class);
 		 JSONObject t1 = response1.getEntity();
 		 response1.releaseConnection();
	     // return t1;
 		JSONObject obj = new JSONObject();
 		obj.put("siteKey", t1.get("siteKey"));
    	
        return obj;
	
    	  } catch (NamingException e) {
  			logger.error("Error al obtener ejb de jndiIFacadeEJB", e);
  	  }  	   catch (Exception e) {
			logger.error("Error general", e);
	  }
    	  return null;
	}
	
	

	    @POST
	    @Path("/validarArchivo")
	    @Consumes(MediaType.MULTIPART_FORM_DATA)
	    @Produces(MediaType.APPLICATION_JSON)
	    public Resultado validarArchivo(MultipartFormDataInput input,@Context final HttpServletRequest request,@Context final HttpServletResponse responde) throws IOException, ServletException {
	    	
	    	
	      	
	    	//esto para que no de error de cors
	    	responde.setHeader("Access-Control-Allow-Origin", "*");
	  
	    	
	    	//controlo el tamaño 
	    	Integer tamanioPermitido = Integer.valueOf(ResourceHelper.getConfig("tamanio.permitido.bytes"));
	    
	    	
	    	if (tamanioPermitido < request.getContentLength()){
	    		Resultado resultado = new Resultado();
				resultado.setErrCod("333");
				resultado.setErrDsc(ResourceHelper.getMensaje("error.tamanioArchivo"));
					return resultado;
				}	
	    	
	    	//return new Resultado();
	    	
	    	logger.info("[LOGUEO VALIDADOR]" +"[validarArchivo en FRONTEND][Inicio]");
	    	
	    	
	    	Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
			
	    	
	    	List<InputPart> inputParts3 = uploadForm.get("response");
			String captchaResponse = "";
			  for (InputPart inputPart3 : inputParts3) {
					InputStream inputStream3  = inputPart3.getBody(InputStream.class,
		                     null);
					
					StringWriter writer = new StringWriter();
					IOUtils.copy(inputStream3, writer);
					captchaResponse = writer.toString();
					
			  }
	    	
	    	
			List<InputPart> inputParts2 = uploadForm.get("name");
			String name = "";
			  for (InputPart inputPart2 : inputParts2) {
					InputStream inputStream2  = inputPart2.getBody(InputStream.class,
		                     null);
					
					StringWriter writer = new StringWriter();
					IOUtils.copy(inputStream2, writer);
					name = writer.toString();
					
			  }
			  
			  logger.info("[LOGUEO VALIDADOR]" +"[validarArchivo en FRONTEND][name="+name+ "]");
			  
			  InputStream inputStream = null;
			  List<InputPart> inputParts = uploadForm.get("file");
		        for (InputPart inputPart : inputParts) {
		            try {
		 
		            	//logger.info("[LOGUEO VALIDADOR]" +"[validarArchivo en FRONTEND][inputPart.getBodyAsString()="+inputPart.getBodyAsString()+ "]");
		            	//logger.info("[LOGUEO VALIDADOR]" +"[validarArchivo en FRONTEND][inputPart.getBodyAsString().length="+inputPart.getBodyAsString().length()+ "]");
		            	
		            	
		                MultivaluedMap<String, String> header = inputPart.getHeaders();
		                // convert the uploaded file to inputstream
		                inputStream = inputPart.getBody(InputStream.class,
		                        null);

		            } catch (Exception e) {
		                e.printStackTrace();
		            }
		        }
		        
		       //logger.info("[LOGUEO VALIDADOR]" +"[validarArchivo en FRONTEND][inputStream="+inputStream+ "]");
		       //logger.info("[LOGUEO VALIDADOR]" +"[validarArchivo en FRONTEND][inputStream Size="+inputStream.available()+ "]");
		        	   
		       
	    	try {

			 
			  
			
//        	  try {
//        		 InitialContext ctx = new InitialContext();
//        		 facadeWebEJB = (IFacadeWebEJB) ctx.lookup(ResourceHelper.getConfig("jndiIFacadeWebEJB"));
//        	  } catch (NamingException e) {
//        			logger.error("Error al obtener ejb de jndiIFacadeEJB", e);
//        	  }
          
        	  //RECAPTCHA
        	  ClientRequest request1 = new ClientRequest(facadeWebEJB.getRecaptchaUrl());
        	  request1.accept("application/json");
        	  JSONObject obj2 = new JSONObject();
        	  obj2.put("response", captchaResponse);
    		  request1.body(MediaType.APPLICATION_JSON, obj2);
    		  ClientResponse<JSONObject> response1 = request1.post(JSONObject.class);
    		  JSONObject t1 = response1.getEntity();
    		  response1.releaseConnection();
    		  
    		  Boolean captchaOk = false;
    		  if ("true".equals(t1.get("success"))) {
    			  captchaOk = true;
    		  }
    		  
    		  if (!captchaOk) {
    				Resultado resultado = new Resultado();
    				resultado.setErrCod("444");
    				resultado.setErrDsc(ResourceHelper.getMensaje("error.captchaInvalido"));
    				return resultado;
    		  }
    		  
        	  //VALIDADOR
			  //Simple ClientRequest invocation
	    	  MultipartFormDataOutput mpfdo = new MultipartFormDataOutput();
	          mpfdo.addFormData("name",name, MediaType.TEXT_PLAIN_TYPE);
	    	  mpfdo.addFormData("file",inputStream, MediaType.APPLICATION_OCTET_STREAM_TYPE);
        	  
        	  ClientRequest request2 = new ClientRequest(facadeWebEJB.getValidarArchivoUrl());
        	  request2.accept("application/json");
			  request2.body(MediaType.MULTIPART_FORM_DATA_TYPE, mpfdo);
			  //LogueoAplicacion.logueoInvocacion(logger, facadeWebEJB.getMonitorUrl(), facadeWebEJB.getMonitorUrl(), mpfdo);
			  ClientResponse<Resultado> response2 = request2.post(Resultado.class);
			  Resultado t2 = response2.getEntity();
			  response2.releaseConnection();
			  return t2;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	        return new Resultado();
	    }
	    
	    /* MONITOR*/
	    @Path("/monitor")
	    @GET
	    @Produces(MediaType.APPLICATION_JSON)
	    public JSONObject monitor(@Context final HttpServletRequest request) {
	    	
	    	logger.info("[LOGUEO VALIDADOR]" +"[GET monitor en FRONTEND][Inicio]");
	    	
	    	JSONObject obj = new JSONObject();
	    	obj.put("method", "GET");
	    	obj.put("info", "DMZ");
	        obj.put("status", "ok");
	        
	        return obj;
	    }
	    
	    /* MONITOR*/
	    @Path("/monitor")
	    @POST
	    @Produces(MediaType.APPLICATION_JSON)
	    public JSONObject monitor(JSONObject input,@Context final HttpServletRequest request) {
	    
	      logger.info("[LOGUEO VALIDADOR]" +"[POST monitor en FRONTEND][Inicio]");
		  logger.info("[LOGUEO VALIDADOR]" +"[POST monitor en FRONTEND][input="+input+ "]");
			
	    	
	     JSONObject obj = new JSONObject();
	    	obj.put("method", "POST");
	    	obj.put("info", "DMZ");
	        obj.put("status", "ok");
	        obj.put("input", input.get("input"));
	        
	        
	        
//	      IFacadeWebEJB facadeWebEJB =  null;
//      	  try {
//      		 InitialContext ctx = new InitialContext();
//      		 facadeWebEJB = (IFacadeWebEJB) ctx.lookup(ResourceHelper.getConfig("jndiIFacadeWebEJB"));
//      	  } catch (NamingException e) {
//      			logger.error("Error al obtener ejb de jndiIFacadeEJB", e);
//      	  }
	        
	  	  ClientRequest request2 = new ClientRequest(facadeWebEJB.getMonitorUrl());
    	  request2.accept("application/json");
    	  
    	  JSONObject obj2 = new JSONObject();
    	  obj2.put("input", "input interno");
	        	  
		  request2.body(MediaType.APPLICATION_JSON, obj2);
		  ClientResponse<JSONObject> response2=null;
		  try {
			  
			  
			  LogueoAplicacion.logueoInvocacion(logger, facadeWebEJB.getMonitorUrl(), facadeWebEJB.getMonitorUrl(), obj2);
			  response2 = request2.post(JSONObject.class);
		  } catch (Exception e) {
		  // TODO Auto-generated catch block
				e.printStackTrace();
		  }
		  JSONObject t = response2.getEntity();
		  response2.releaseConnection();

		  obj.put("inputBackend", t.get("input"));
		  
          return obj;
	    }
		
    
	    
	    
	    
	    
	    
	    
}
