/**
 * Disclaimer: this code is only for demo no production use
 */
package bps.atyr.validadorFormatosEv1.ejb;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import bps.atyr.acerca.utils.ResourceHelper;
//import bps.atyr.acerca.validadorFormatosEv1.common.ejb.interfaces.IFacadeWebEJB;

/**
 * @author papo
 *
 */
@Stateless

public class FacadeWebEJB{


    public String getValidarArchivoUrl(){
    	return ResourceHelper.getConfig("backend.validarArchivo.url");
    }
    public String getMonitorUrl(){
    	return ResourceHelper.getConfig("backend.monitor.url");
    }

    public String getRecaptchaUrl(){
    	return ResourceHelper.getConfig("backend.validarRecaptcha.url");
    }
    
    public String getSiteKeyUrl(){
    	return ResourceHelper.getConfig("backend.siteKey.url");
    }
    
		
}
