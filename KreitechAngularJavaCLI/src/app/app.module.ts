import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ElementRef } from '@angular/core';
import { HttpModule } from '@angular/http';
import { ReCaptchaModule } from 'angular2-recaptcha';

//ROUTES
import {APP_ROUTING} from './app.routes';

//SERVERS
import { ValidadorService } from './services/validador/validador.service';
//import { VerificarCaptchaService } from './services/verificarCaptcha/verificarCaptcha.service';



import { SpinnerService } from './services/spinner/spinner.service';
import { ConfService } from './services/conf/conf.service'

//COMPONENTS
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ResultadoComponent } from './components/resultado/resultado.component';
import { SpinnerComponent } from './components/spinner/spinner.component';

//PIPES
import { NombreArchivo } from './pipes/nombreArchivo.pipe';
import { ArchivoNoSeleccionado } from './pipes/archivoNoSeleccionado.pipe'

@NgModule({
  declarations: [
  ArchivoNoSeleccionado,
    NombreArchivo,
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AboutComponent,
    ResultadoComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    HttpModule,
    ReCaptchaModule
  ],
  providers: [ValidadorService,SpinnerService,ConfService],
  bootstrap: [AppComponent]
})
export class AppModule { }
