import { Injectable} from '@angular/core';

@Injectable()
export class SpinnerService{

    constructor(){}
spinnerOn=false;

    ocultarSpinner(){
      this.spinnerOn=false;
    }
    mostrarSpinner(){
      this.spinnerOn=true;
      
    }

    isOnSpinner():boolean{
      return this.spinnerOn;
    }

}
