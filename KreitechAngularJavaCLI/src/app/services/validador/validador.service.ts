import { Injectable} from '@angular/core';
import { Http, Headers,RequestOptions,Response} from '@angular/http';
import { environment } from '../../../environments/environment';
import 'rxjs/Rx';
import {ConfService} from '../conf/conf.service';
import {Observable} from "rxjs/Observable";

@Injectable()
export class ValidadorService{
//validarURL:string = "http://localhost:8080/ValidadorFormatosWebEv1WS/rest/utilitarios/validarArchivo"
resultado:any={};
fileList: FileList=null;
config:any;
validarURL:string;
siteKeyURL:string;
responseCaptcha:string;

constructor(private http:Http){
}

public handleError(error: Response) {
    console.error(error);
    return Observable.of({"errCod":"555","errDsc":"Error de conexión. Disculpe las molestias"});
}

validarArchivo(config:any){
  this.config = config;

   this.validarURL =`${ this.config.apiProtocol }://${ this.config.apiHost }:${ this.config.apiPort }/${ this.config.apiContextRoot }/rest/utilitarios/validarArchivo`;

  if(this.fileList !=null && this.fileList.length > 0) {
    let file: File = this.fileList[0];
    let formData:FormData = new FormData();
    formData.append('file', file);
    formData.append('name', file.name);
    formData.append('response', this.responseCaptcha);
//    console.log("this.responseCaptcha="+this.responseCaptcha)
    let bodyString = JSON.stringify(file); // Stringify payload
    let headers      = new Headers({});

    let options       = new RequestOptions({ headers: headers }); // Create a request option

    return this.http.post(this.validarURL,formData, options).map( res =>{
        //console.log( res.json()  );
        return res.json();
    }).catch(this.handleError);
    };
    }

    obtenerResultado(){
      return this.resultado;
    }

    setArchivo(fileListIn: FileList){
      this.fileList=fileListIn;
    }

    setResponseCaptcha(response: string){
      this.responseCaptcha=response;
    }

    getArchivo(){
      return this.fileList;
    }

    obtenerSiteKey(config:String){
      this.config=config;
      this.siteKeyURL =`${ this.config.apiProtocol }://${ this.config.apiHost }:${ this.config.apiPort }/${ this.config.apiContextRoot }/rest/utilitarios/site-key`;
    //this.siteKeyURL = "http://recaptchat.bps.gub.uy:8080/ReCaptchaWS/rest/v1/site-key"
    //console.log("this.siteKeyURL="+this.siteKeyURL);
      return this.http.get(this.siteKeyURL)
      .map(
      res =>{
        return res.json();
      })
      .catch(this.handleError);
    }



}
