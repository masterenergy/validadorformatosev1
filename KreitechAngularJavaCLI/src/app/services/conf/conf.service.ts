import { Injectable} from '@angular/core';
import { Http, Headers,RequestOptions} from '@angular/http';
import { environment } from '../../../environments/environment';
import 'rxjs/Rx';
import {Observable} from "rxjs/Observable";

@Injectable()
export class ConfService{

res:any={};
constructor(private http:Http){
}

config:any = {
  "apiProtocol":"https",
  "apiHost":"validadorformatost.bps.gub.uy",
  //"apiHost":"localhost",
  "apiPort":443,
  //"apiPort":8080,
  "apiContextRoot":"ValidadorFormatosWebEv1WS",
  //"reCaptchaProtocol":"https",
  //"reCaptchaHost":"recaptchat.bps.gub.uy",
  //"reCaptchaPort":443,
  //"reCaptchaContextRoot":"ReCaptchaWS",
  "domain":"bps.net",
};

getConf(){
  if (!environment.mockConfig){
    //console.log("prodConfig");
    return this.getConfTestProd();
  }else{
    //console.log("mockConfig");
    return this.getConfDev();
  }
}

getConfDev(){

return Observable.of(this.config);

}

getConfTestProd(){
      var url = window.location.href
      var arr = url.split("/");
      var myhostandport = arr[0] + "//" + arr[2]
      var urlconfig = "";
      //urlconfig = "http://localhost:8080/ValidadorFormatosWebEv1WS/rest/utilitarios/config.json"
      //urlconfig = "http://fso13p0232w076.bps.net:8080/ValidadorFormatosWebEv1.conf/config.json"
      urlconfig = myhostandport+"/ValidadorFormatosWebEv1.conf/config.json";
      //console.log("urlconfig"+urlconfig);

      return this.http.get(urlconfig)
      .map(
        res =>{
            //console.log( "vino=" +res.json());
            return res.json();
      });
    }
}
