import { Component, OnInit } from '@angular/core';
import { ValidadorService } from '../../services/validador/validador.service';
import {SpinnerComponent} from '../spinner/spinner.component';
import 'rxjs/Rx';
import {ConfService} from '../../services/conf/conf.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  //directives: [SpinnerComponent],
  //template: '<my-spinner [isRunning]="isRequesting"></my-spinner>'
})
export class ResultadoComponent implements OnInit {

  public isRequesting: boolean;
  config:any;

  resultado:any={};

  constructor(  private _router:Router,private _validadorService:ValidadorService ,private confService:ConfService) {

     //this.refresh();
  }

   ngOnInit() {
    //console.log("ngOnInit RESULTADO");
    //si no tengo archivo es por di F5, entonces redirijo al home
    if (this._validadorService.getArchivo() ==null) {
    this._router.navigate(['home.spa']);
  }else{

  //TODO: hacer eso singleton

    this.isRequesting = true;
    this.confService.getConf().subscribe(
    res=> {
      this.config=res;
      this._validadorService.validarArchivo(res).subscribe(
      res=> {this.resultado=res;
      this.stopRefreshing();
      //console.log("seting timeout")


      //console.log(JSON.stringify(res));
      });
    }
    )//.catch(this.handleError);;
}
}


//promise = new Promise(
//function(resolve,reject){
//this._validadorService.validarArchivo().subscribe(res=>{
//this.resultado = res;
//console.log("sadas");
//resolve();

//});

//}
//);


public handleError(error: Response) {
    console.error("el servidor no está respondiendo");
    //return Observable.throw(error.json().error || 'Server error');
}



public refresh(): void {


}



private stopRefreshing() {
    this.isRequesting = false;
    //despues que detiene el refreshing hay que redimensionar la pantalla.
    //TODO: encontrar una mejor solución para esto

    setTimeout(()=>{
      var frameEsc:any = window.parent.document.getElementById("frame");
      //console.log("frameEsc="+frameEsc)
      if (frameEsc!=null){
        let win:any= window.parent;
        win.resizeIframe(frameEsc)
      }
    }, 50);

    ///por las dudas a¿hago lo mismo un segundo después
    setTimeout(()=>{
      var frameEsc:any = window.parent.document.getElementById("frame");
      //console.log("frameEsc="+frameEsc)
      if (frameEsc!=null){
        let win:any= window.parent;
        win.resizeIframe(frameEsc)
      }
    }, 1000);


}

/*
imprimir() {
    window.print();
}
*/
imprimir() {
  if (document.queryCommandSupported('print')) {
    document.execCommand('print', false, null);
  }else {
    window.print();
  }
}


//TODO: esto tiene que estar en un solo lugar
ngAfterViewInit(){
    //console.log("ngAfterViewInit")
    try {
       var frameEsc:any = window.parent.document.getElementById("frame");
       //console.log("frameEsc="+frameEsc)
       if (frameEsc!=null){
         let win:any= window.parent;
         win.resizeIframe(frameEsc)
       }
    }catch(err) {
      console.log(err)
    }
}

}
