import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers,RequestOptions} from '@angular/http';
import { ValidadorService } from '../../services/validador/validador.service';
//import { VerificarCaptchaService } from '../../services/verificarCaptcha/verificarCaptcha.service';
import { SpinnerService } from '../../services/spinner/spinner.service';

import { ViewChild } from '@angular/core';
import { ReCaptchaComponent } from 'angular2-recaptcha';
import {ConfService} from '../../services/conf/conf.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  fileList: FileList=null;
  errores=[];
  name:string="";
  config:any;

  @ViewChild(ReCaptchaComponent) captcha: ReCaptchaComponent;
  siteKey:string;

  public isRequesting: boolean;

  ngOnInit() {


    //TODO: hacer eso singleton
    this.confService.getConf().subscribe(
    res=> {
      this.config=res;
      this._validadorService.obtenerSiteKey(res).subscribe(
          res=> {
            this.siteKey=res.siteKey;
            //console.log("siteKey en conponents ts"+this.siteKey);
          },
          err => { //error handling
            //console.log(err);
            this.errores=this.errores.concat(["Error Interno. Disculpe las molestias."]);
          }
        );
    }
    );
 }

  constructor(private spinnerService:SpinnerService,
  private _validadorService:ValidadorService,
  //private _verificarCaptchaService:VerificarCaptchaService,
  private _router:Router
,private confService:ConfService
) {
  };

  fileChange(event){
    this.fileList =event.target.files;
    if(this.fileList.length > 0) {
      this.name=this.fileList[0].name;
    }
  }

verResultados(){

 this.errores=[];

 if (this.captcha.getResponse()==""){
    this.errores=this.errores.concat(["Debe dar click en el captcha"]);
 }
 if (this.fileList===null){
    this.errores=this.errores.concat(["Debe seleccionar un archivo"]);
 }

  if (this.errores.length == 0){
    this.isRequesting = true;
          this._validadorService.setArchivo(this.fileList);
          this._validadorService.setResponseCaptcha(this.captcha.getResponse().toString());
          this._router.navigate(['resultado.spa']);
    };
  };

    limpiar(){
      this.name="";
      this.fileList = null;
      //window.location.reload();
    }


    //TODO: esto tiene que estar en un solo lugar
    ngAfterViewInit(){
        //console.log("ngAfterViewInit")
        try {
           var frameEsc:any = window.parent.document.getElementById("frame");
           //console.log("frameEsc="+frameEsc)
           if (frameEsc!=null){
             let win:any= window.parent;
             win.resizeIframe(frameEsc)
           }
        }catch(err) {
          console.log(err)
        }
    }






}
