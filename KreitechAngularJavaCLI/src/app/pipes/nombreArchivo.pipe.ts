import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nombreArchivo'
})
export class NombreArchivo implements PipeTransform {

  transform(value: string, cantidad:number = 10 ): string {
    if (!value){
      return value;
    }

    if (value.length < 20){
      return value;
    }
    let head:string = value.substring(0, 10);
    let tail:string  = value.substring( value.length-10, value.length);
    return head.concat( "...").concat( tail);

  }

}
