import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'archivoNoSeleccionado'
})
export class ArchivoNoSeleccionado implements PipeTransform {

  transform(value: string): string {


    if (value==''){
      return "Archivo no seleccionado";
    }
    return value;
  }

}
