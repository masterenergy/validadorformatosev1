import { RouterModule, Routes } from '@angular/router';

import {HomeComponent} from './components/home/home.component';
import {AboutComponent} from './components/about/about.component';
import {ResultadoComponent} from './components/resultado/resultado.component';

const APP_ROUTES: Routes = [
  { path: 'home.spa', component: HomeComponent },
  { path: 'about.spa', component: AboutComponent },
  { path: 'resultado.spa', component: ResultadoComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home.spa' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
