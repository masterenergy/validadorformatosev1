import { Component } from '@angular/core';
//import { LoaderService } from './shared/service/loader.service';
import { SpinnerService } from './services/spinner/spinner.service';
import {ConfService} from './services/conf/conf.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'app';
  constructor(private confService:ConfService,private spinnerService:SpinnerService) {

  };
  resolved(captchaResponse: string) {
          console.log(`Resolved captcha with response ${captchaResponse}:`);
      }

  ngOnInit() {
    //console.log("ngOnInit")
    //TODO: hacer eso singleton
    this.confService.getConf().subscribe(
    (res)=> {
        document.domain=res.domain;
        //console.log("domain="+res.domain);
    }
    );

  }


}
